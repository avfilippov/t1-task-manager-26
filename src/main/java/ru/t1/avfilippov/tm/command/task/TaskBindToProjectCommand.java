package ru.t1.avfilippov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "bind task to project";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-bind-to-project";
    }

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @Nullable final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @Nullable final String taskId = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        getProjectTaskService().bindTaskToProject(userId, projectId, taskId);
    }

}
